﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contact us.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contact Us</title>

<script>
	msg = "The project for school!";

	msg = "..." + msg;
	position = 0;
	function scrollMSG() {
	document.title = msg.substring(position, msg.length) + msg.substring(0, position);
	position++;
	if (position >  msg.length) position = 0
	window.setTimeout("scrollMSG()",200);
	}
	scrollMSG();

	
</script>

<style>
	h1
	{
		font-family:Verdana, Geneva, sans-serif;
		color:#ffff00;
	}
	h1:hover 
	{
		color:black;
	}
	#first
	{
		text-align:center;
		font-size:125%;
		color:gray;
		font-family: sans-serif;
		margin:0;
		
	}
	#second
	{
		text-align:center;
		font-size:125%;
		color:red;
		font-family: sans-serif;
		
	}
	#third{
		background-color: #5A3314;
		color: white;
		padding: 12px 18px;
		margin: 6px 0;
		border: none;
		cursor: pointer;
		width: 100%;
		font-family:Impact, Charcoal, sans-serif;
	}
	#third:hover {
		opacity: 0.6;
	}
	h3
	{
		background-color: #5A3314;
		color: white;
		padding: 16px 20px;
		margin: 10px 0;
		border: none;
		cursor: default;
		width: 100%;
		font-family:"Comic Sans MS", cursive, sans-serif;
		font-size:125%;
		
	}
	#contact_form
	{
		
		font-size:200%;
		text-align:center;
		background-color: #5A3314;
		color: white;
		padding: 16px 20px;
		margin: 10px 0;
		border: none;
		cursor: default;
		width: 100%;
		font-family:"Comic Sans MS", cursive, sans-serif;
	}
	#submit_button{
		background-color: #5A3314;
		color: white;
		padding: 12px 18px;
		margin: 6px 0;
		border: none;
		cursor: pointer;
		font-size:150%;
		font-family:Impact, Charcoal, sans-serif;
	}
	#reset_button{
		background-color: #5A3314;
		color: white;
		padding: 12px 18px;
		margin: 6px 0;
		border: none;
		cursor: pointer;
		font-size:150%;
		font-family:Impact, Charcoal, sans-serif;
	}
</style>
</head>
<body background="http://i.stack.imgur.com/jGlzr.png">

    <div id="first">
		<div >
			<img src="http://img11.deviantart.net/4c36/i/2016/040/7/d/pc_master_race_by_kingvego-d9r6gtn.png" alt="Jon Snow" width="200" height="200"/>
		</div>
		<h1>Master PC Race<br/>The Best Place To See the Besties</h1>
		<div id="second"> 
			<a style="text-decoration:none" id = "third" href="about us.html" >About</a>
			<a style="text-decoration:none" id = "third" href="#" >Contact Us</a>
			<a style="text-decoration:none" id = "third" href="home.html">Home</a>
		</div>
		<h3>Contact Us:</h3>
	</div>
	
    <form id ="contact_form" action="mailto:koren017@gmail.com" method="post" enctype="text/plain">
        First name*<br/>
        <input type="text" name="firstname" value="" size="30" required="required" pattern="[a-zA-Z]{2-12}" title="Only 2 to 12 latters!"/><br/>
        Last name*<br/>
        <input type="text" name="lastname" value="" size="30" required="required" pattern="[a-zA-Z] {2-12}" title="Only 2 to 12 latters!"/><br/>

        Gender:<br/>
        <input type="radio" name="gender" value="male"/> Male<br/>
        <input type="radio" name="gender" value="female"/> Female<br/>
        <input type="radio" name="gender" value="other" checked="checked"/> Other<br/>

        
        <label for="email">Your email:</label><br />
        <input id="email" class="input" name="email" type="text" value="" size="30" autocomplete="on" /><br />
     
       
        <label for="message">Your message*</label><br />
        <textarea id="message" class="input" name="message" rows="7" cols="30" required="required"/></textarea><br />
 
        <input id="submit_button" type="submit" value="Send email" />
        <input id="reset_button" type="reset"/>
    </form>		
</body>
</html>
